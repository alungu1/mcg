<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use App\Traits\FullTextSearch;

class Experience extends Model
{
    use HasFactory, FullTextSearch;

    protected $fillable = ['name', 'description', 'video_path', 'rating', 'year', 'hero', 'bg_image'];
    protected $appends = ['hero_path', 'bg_image_path'];

    public function getHeroPathAttribute()
    {
        return Storage::url('images/' . $this->hero);
    }

    public function getBgImagePathAttribute()
    {
        return Storage::url('images/' . $this->bg_image);
    }

    public function scopeWhenSearch($query, $search)
    {
        return $query->when($search, function ($q) use ($search) {
            return $q->where('name', 'like', "%$search%")
                ->orWhere('description', 'like', "%$search%")
                ->orWhereRaw("MATCH (description) AGAINST (? IN BOOLEAN MODE)" , $this->tokenizeForFullTextSearch($search))
                ->orWhere('year', 'like', "%$search%")
                ->orWhere('rating', 'like', "%$search%")
                // Bring back categories matching the search criteria
                ->orWhereHas('categories', function($category) use ($search) {
                    return $category->where('name', 'LIKE', "%$search%");
                });
        });
    }


    public function scopeWhenCategory($query, $category)
    {
        return $query->when($category, function ($q) use ($category) {

            return $q->whereHas('categories', function ($qu) use ($category) {

                return $qu->whereIn('category_id', (array)$category)
                    ->orWhereIn('name', (array)$category);

            });

        });
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'experience_category');
    }
}
