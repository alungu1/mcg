<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Experience;

class ExperienceController extends Controller
{
    public function index()
    {
        if (request()->ajax()) {
            $experiences = Experience::whenSearch(request()->search)->get();
            return $experiences;
        }

        $experiences = Experience::whenCategory(request()->category_name)
            ->whenSearch(request()->search)
            ->paginate(20);

        return view('experiences.index', compact('experiences'));
    }

    public function show(Experience $experience)
    {
        $related_experiences = Experience::where('id', '!=', $experience->id)
            ->whereHas('categories', function ($query) use ($experience) {
                return $query->whereIn('category_id', $experience->categories->pluck('id')->toArray());
            })->get();

        return view('experiences.show', compact('experience', 'related_experiences'));
    }

    public function increment_views(Experience $experience)
    {
        $experience->increment('views');
    }

    public function toggle_favorite(Experience $experience)
    {
        //Implement User Model then add relation to table
        return true;
    }
}
