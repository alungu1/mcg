<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Experience;
use App\Models\Category;

class WelcomeController extends Controller
{
    public function index()
    {
        $latest_experiences = Experience::latest()->limit(5)->get();
        $categories = Category::with('experiences')->get();

        return view('welcome', compact('latest_experiences', 'categories'));
    }
}
