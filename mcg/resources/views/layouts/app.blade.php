<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Learning Experiences</title>

    <!--bootstrap-->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">

    <!--owl carousel css-->
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.theme.default.min.css') }}">

    <!--google font-->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,500,700&display=swap" rel="stylesheet">

    <!--main styles -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">

    {{--easy auto complete--}}
    <link rel="stylesheet" href="{{ asset('css/easy-autocomplete.min.css') }}">
    <style>
        .fw-900 {
            font-weight: 900;
        }
        .easy-autocomplete {
            width: 90%;
        }
        .easy-autocomplete input {
            color: white !important;
        }
        .eac-icon-left .eac-item img {
            max-height: 80px !important;
        }
    </style>
</head>
<body>

@yield('content')

<!--jquery-->
<script src="{{ asset('js/jquery.min.js') }}"></script>

<!--bootstrap-->
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/popper.min.js') }}"></script>

<!--vendor js-->
<script src="{{ asset('js/owl.carousel.min.js') }}"></script>

<!--main scripts-->
<script src="{{ asset('js/site.js') }}"></script>

<!--easy autocomplete-->
<script src="{{ asset('js/jquery.easy-autocomplete.min.js') }}"></script>

<!-- custom experiences-->
<script src="{{ asset('js/experience.js') }}"></script>

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var options = {
        url: function (search) {
            return "/experiences?search=" + search;
        },
        getValue: "name",
        requestDelay: 100,
        template: {
            type: 'iconLeft',
            fields: {
                iconSrc: "hero_path"
            }
        },
        list: {
            onChooseEvent: function () {
                var experience = $('.form-control[type="search"]').getSelectedItemData();
                var url = window.location.origin + '/experiences/' + experience.id;
                window.location.replace(url);
            }
        }
    };
    $('.form-control[type="search"]').easyAutocomplete(options)
    $(document).ready(function () {
        $("#banner .experiences").owlCarousel({
            loop:true,
            items: 1,
            autoplay:true,
            autoplayTimeout:5000,
            autoplayHoverPause:true,
            dots: true,
            smartSpeed :900,
        });
        $(".listing .experiences").owlCarousel({
            loop: true,
            nav: true,
            stagePadding: 50,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 2
                },
                1000: {
                    items: 4
                }
            },
            dots: false,
            margin: 15,
            loop: true,
        });
    });
</script>

@stack('scripts')

</body>
</html>
