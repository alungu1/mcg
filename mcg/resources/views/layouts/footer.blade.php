<footer id="footer" class="py-3 bg-primary text-center text-white">
    <p class="mb-0 text-capitalize">&copy; {{ now()->year . ' ' . config('app.name')}}</p>
</footer>
