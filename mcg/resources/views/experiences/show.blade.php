@extends('layouts.app')

@section('content')

    <section id="show">

        @include('layouts.nav')

        <div class="experience">

            <div class="experience__bg" style="background: linear-gradient(rgba(0,0,0, 0.6), rgba(0,0,0, 0.6)), url({{ $experience->bg_image_path }}) center/cover no-repeat;"></div>

            <div class="container">

                <div class="row">

                    <div class="col-md-8 ">

                    <video id="experience-video"
                        class="video-js"
                        width="100%"
                        height="100%"
                        controls=true,
                        poster="{{ $experience->hero_path}}"
                        preload="auto"
                        data-setup='{"autoplay": false}'>
                        <source src="{{$experience->video_path}}">
                    </video>

                    </div><!-- end of col -->

                    <div class="col-md-4 text-white">
                        <h3 class="experience__name fw-300">{{ $experience->name }}</h3>

                        <div class="d-flex experience__rating my-1">
                            <div class="d-flex mr-2">
                                <span class="score mr-2"><span class="text-primary" style="width: {{ $experience->rating <= 5 ? ($experience->rating / 5) * 100 : 100}}%"></span></span>
                            </div>
                            <span class="align-self-center">{{ $experience->rating }}</span>
                        </div>

                        <p>Views: <span id="experience__views">{{ $experience->views }}</span></p>

                        <p class="experience__description my-3">
                            {!! $experience->description !!}
                        </p>
                            <a href="#" class="btn btn-primary text-capitalize experience__fav-btn">
                                <span class="fas fa-heart experience__fav-icon experience-{{ $experience->id }} {{ $experience->is_favored ? 'fw-900' : '' }}"
                                      data-experience-id="{{ $experience->id }}"
                                      data-url="{{ route('experiences.toggle_favorite', $experience->id) }}"
                                >
                                </span>
                                add to favorite
                            </a>

                    </div><!-- end of col -->

                </div><!-- end of row -->

            </div><!-- end of container -->

        </div><!-- end of experience -->

    </section><!-- end of banner section-->

    <section class="listing py-2">

        <div class="container">

            <div class="row my-4">
                <div class="col-12 d-flex justify-content-between">
                    <h3 class="listing__title text-white fw-300">Related Experiences</h3>
                </div>
            </div><!-- end of row -->

            <div class="experiences owl-carousel owl-theme">

                @foreach ($related_experiences as $related_experience)

                    <div class="experience p-0">
                        <img src="{{ $related_experience->hero_path }}" class="img-fluid" alt="">

                        <div class="experience__details text-white">

                            <div class="d-flex justify-content-between">
                                <p class="mb-0 experience__name">{{ $related_experience->name }}</p>
                                <p class="mb-0 experience__year align-self-center">{{ $related_experience->year }}</p>
                            </div>

                            <div class="d-flex experience__rating">
                                <div class="mr-2">
                                    @for ($i = 0; $i < $related_experience->rating; $i++)
                                        <i class="fas fa-star text-primary mr-1"></i>
                                    @endfor
                                </div>
                                <span>{{ $related_experience->rating }}</span>
                            </div>

                            <div class="experience___views">
                                <p>Views: {{ $related_experience->views }}</p>
                            </div>

                            <div class="d-flex experience__cta">
                                <a href="{{ route('experiences.show', $related_experience->id) }}" class="btn btn-primary text-capitalize flex-fill mr-2"><i class="fas fa-play"></i> watch now</a>
                                <i class="far fa-heart {{ $related_experience->is_favored ? 'fw-900' : ''}} fa-1x align-self-center experience__fav-icon experience-{{ $related_experience->id }}"
                                   data-experience-id="{{ $related_experience->id }}"
                                   data-url="{{ route('experiences.toggle_favorite', $related_experience->id) }}"
                                >
                                </i>
                            </div>

                        </div><!-- end of experience details -->

                    </div><!-- end of col -->

                @endforeach

            </div><!-- end of row -->

        </div><!-- end of container -->

    </section><!-- end of listing section -->

    @include('layouts.footer')

@endsection

@push('scripts')

    <script>
        var player = document.getElementById('experience-video');
        player.addEventListener('timeupdate', incrementViews , false);

        let viewsCounted = false;

        function incrementViews(){
            let percent = (player.currentTime / player.duration) * 100;

            if (percent > 10 && !viewsCounted) {
                $.ajax({
                    url: "{{ route('experiences.increment_views', $experience->id) }}",
                    method: 'POST',
                    success: function () {
                        let views = parseInt($('#experience__views').html());
                        $('#experience__views').html(views + 1);
                    },
                });
                viewsCounted = true;
            }
        }
    </script>

@endpush
