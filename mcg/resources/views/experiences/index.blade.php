@extends('layouts.app')

@section('content')

    <section class="listing" style="height: 100vh; padding: 8% 0;">

        @include('layouts.nav')

        <div class="container text-white">

            <div class="row">

                <div class="col">

                    <h2 class="fw-300">{{ request()->category_name ?? 'Favorite' }} Experiences</h2>

                </div><!-- end of col -->

            </div><!-- end of row -->

            <div class="row {{ request()->favorite ? 'favorite' : '' }}">

                @if ($experiences->count() > 0)

                    @foreach ($experiences as $experience)

                        <div class="experience col-md-3  my-3 p-0">
                            <img src="{{ $experience->hero_path }}" class="img-fluid" alt="">

                            <div class="experience__details text-white">

                                <div class="d-flex justify-content-between">
                                    <p class="mb-0 experience__name">{{ $experience->name }}</p>
                                    <p class="mb-0 experience__year align-self-center">{{ $experience->year }}</p>
                                </div>

                                <div class="d-flex experience__rating">
                                    <div class="mr-2">
                                        <span class="score mr-1"><span class="text-primary" style="width: {{ $experience->rating <= 5 ? ($experience->rating / 5) * 100 : 100}}%"></span></span>
                                    </div>
                                    <span>{{ $experience->rating }}</span>
                                </div>

                                <div class="experience___views">
                                    <p>Views: {{ $experience->views }}</p>
                                </div>

                                <div class="d-flex experience__cta">
                                    <a href="{{ route('experiences.show', $experience->id) }}" class="btn btn-primary text-capitalize flex-fill mr-2"><i class="fas fa-play"></i> watch now</a>
                                        <i class="fas fa-heart {{ $experience->is_favored ? 'fw-900' : ''}} fa-1x align-self-center experience__fav-icon experience-{{ $experience->id }}"
                                           data-experience-id="{{ $experience->id }}"
                                           data-url="{{ route('experiences.toggle_favorite', $experience->id) }}"
                                        >

                                        </i>
                                </div>

                            </div><!-- end of experience details -->

                        </div><!-- end of col -->

                    @endforeach

                @else

                    <div class="col">
                        <h5 class="fw-300">Sorry no experiences found</h5>
                    </div>

                @endif

            </div><!-- end of row -->

        </div><!-- end of container -->

    </section><!-- end of listing -->

@endsection
