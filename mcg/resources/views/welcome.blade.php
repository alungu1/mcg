@extends('layouts.app')

@section('content')

    <section id="banner">

        @include('layouts.nav')

        <div class="experiences owl-carousel owl-theme">

            @foreach ($latest_experiences as $latest_experience)
                <div class="experience text-white d-flex justify-content-center align-items-center">

                    <div class="experience__bg" style="background: linear-gradient(rgba(0,0,0, 0.6), rgba(0,0,0, 0.6)), url({{$latest_experience->bg_image_path}}) center/cover no-repeat;"></div>

                    <div class="container">

                        <div class="row">

                            <div class="col-md-6">

                                <div class="d-flex justify-content-between">
                                    <h1 class="experience__name fw-300">{{ $latest_experience->name }}</h1>
                                    <span class="experience__year align-self-center">{{ $latest_experience->year }}</span>
                                </div>

                                <div class="d-flex experience__rating my-1">
                                    <div class="d-flex">
                                        <span class="score  mr-2"><span class="text-primary" style="width: {{ $latest_experience->rating <= 5 ? ($latest_experience->rating / 5) * 100 : 100}}%"></span></span>
                                    </div>
                                    <span class="align-self-center">{{ $latest_experience->rating }}</span>
                                </div>

                                <p class="experience__description my-2">
                                    {{ $latest_experience->description }}
                                </p>

                                <div class="experience__cta my-4">
                                    <a href="{{ route('experiences.show', $latest_experience->id) }}" class="btn btn-primary text-capitalize mr-0 mr-md-2"><span class="fas fa-play"></span> view now</a>

                                    <a href="#" class="btn btn-outline-light text-capitalize experience__fav-btn">
                                        <span class="fas fa-heart experience__fav-icon experience-{{ $latest_experience->id }} {{ $latest_experience->is_favored ? 'fw-900' : '' }}"
                                              data-experience-id="{{ $latest_experience->id }}"
                                              data-url="{{ route('experiences.toggle_favorite', $latest_experience->id) }}"
                                        >
                                        </span>
                                        add to favorite
                                    </a>
                                </div>
                            </div><!-- end of col -->

                            <div class="col-6 mt-2 mx-auto col-md-4 col-lg-3  ml-md-auto mr-md-0">
                                <img src="{{ $latest_experience->hero_path }}" class="img-fluid" alt="">
                            </div>
                        </div><!-- end of row -->

                    </div><!-- end of container -->

                </div><!-- end of experience -->

            @endforeach

        </div><!-- end of experiences -->

    </section><!-- end of banner section-->

    @foreach ($categories as $category)

        <section class="listing py-2">

            <div class="container">

                <div class="row my-4">
                    <div class="col-12 d-flex justify-content-between">
                        <h3 class="listing__title text-white fw-300">{{ $category->name }}</h3>
                        <a href="{{ route('experiences.index', ['category_name' => $category->name]) }}" class="align-self-center text-capitalize text-primary">see all</a>
                    </div>
                </div><!-- end of row -->

                <div class="experiences owl-carousel owl-theme">

                    @foreach ($category->experiences as $experience)

                        <div class="experience p-0">
                            <img src="{{ $experience->hero_path }}" class="img-fluid" alt="">

                            <div class="experience__details text-white">

                                <div class="d-flex justify-content-between">
                                    <p class="mb-0 experience__name">{{ $experience->name }}</p>
                                    <p class="mb-0 experience__year align-self-center">{{ $experience->year }}</p>
                                </div>

                                <div class="d-flex experience__rating">
                                    <div class="mr-2">
                                        <span class="score">
                                            <span class="text-primary" style="width: {{ $experience->rating <= 5 ? ($experience->rating / 5) * 100 : 100}}%"></span>
                                        </span>
                                    </div>
                                    <span>{{ $experience->rating }}</span>
                                </div>

                                <div class="experience___views">
                                    <p>Views: {{ $experience->views }}</p>
                                </div>

                                <div class="d-flex experience__cta">
                                    <a href="{{ route('experiences.show', $experience->id) }}" class="btn btn-primary text-capitalize flex-fill mr-2"><i class="fas fa-play"></i> view now</a>
                                    <i class="fas fa-heart {{ $experience->is_favored ? 'fw-900' : ''}} fa-1x align-self-center experience__fav-icon experience-{{ $experience->id }}"
                                       data-experience-id="{{ $experience->id }}"
                                       data-url="{{ route('experiences.toggle_favorite', $experience->id) }}"
                                    >

                                    </i>
                                </div>

                            </div><!-- end of experience details -->

                        </div><!-- end of col -->

                    @endforeach

                </div><!-- end of row -->

            </div><!-- end of container -->

        </section><!-- end of listing section -->

    @endforeach

    @include('layouts.footer')

@endsection
