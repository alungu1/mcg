$(document).ready(function () {

    let favCount = $('#nav__fav-count').data('fav-count');

    $(document).on('click', '.experience__fav-icon', function () {

        let url = $(this).data('url');
        let experienceId = $(this).data('experience-id');
        let isFavored = $(this).hasClass('fw-900');

        toggleFavorite(url, experienceId, isFavored);

    });//end of on click fav icon

    $(document).on('click', '.experience__fav-btn', function () {

        let url = $(this).find('.experience__fav-icon').data('url');
        let experienceId = $(this).find('.experience__fav-icon').data('experience-id');
        let isFavored = $(this).find('.experience__fav-icon').hasClass('fw-900');

        toggleFavorite(url, experienceId, isFavored);

    });//end of on click fav icon

    function toggleFavorite(url, experienceId, isFavored) {

        !isFavored ? favCount++ : favCount--;
        favCount > 9 ? $('#nav__fav-count').html('9+') : $('#nav__fav-count').html(favCount);

        $('.experience-' + experienceId).toggleClass('fw-900');

        if ($('.experience-' + experienceId).closest('.favorite').length) {

            $('.experience-' + experienceId).closest('.experience').remove();

        }//end of if

        $.ajax({
            url: url,
            method: 'POST',
        });//end of ajax call

    }

});//end of document ready
