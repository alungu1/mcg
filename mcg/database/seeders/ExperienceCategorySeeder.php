<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ExperienceCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $experiences = \DB::table('experiences')->get()->pluck('id');
        $first_category = \DB::table('categories')->first();

        foreach ($experiences as $id) {
            \DB::table('experience_category')->insert([
                'experience_id' => $id,
                'category_id' => $first_category->id
            ]);
        }
    }
}
