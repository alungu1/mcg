<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ExperienceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('experiences')->insert([
        [
            'name' => 'Learn PHP',
            'description' => 'PHP is a general-purpose scripting language especially suited to web development. It was originally created by Danish-Canadian programmer Rasmus Lerdorf in 1994. The PHP reference implementation is now produced by The PHP Group. PHP originally stood for Personal Home Page, but it now stands for the recursive initialism PHP: Hypertext Preprocessor.\n\nPHP code is usually processed on a web server by a PHP interpreter implemented as a module, a daemon or as a Common Gateway Interface (CGI) executable. On a web server, the result of the interpreted and executed PHP code – which may be any type of data, such as generated HTML or binary image data – would form the whole or part of an HTTP response. Various web template systems, web content management systems, and web frameworks exist which can be employed to orchestrate or facilitate the generation of that response. Additionally, PHP can be used for many programming tasks outside of the web context, such as standalone graphical applications and robotic drone control. Arbitrary PHP code can also be interpreted and executed via command-line interface (CLI).\n\nThe standard PHP interpreter, powered by the Zend Engine, is free software released under the PHP License. PHP has been widely ported and can be deployed on most web servers on almost every operating system and platform, free of charge.\n\nThe PHP language evolved without a written formal specification or standard until 2014, with the original implementation acting as the de facto standard which other implementations aimed to follow. Since 2014, work has gone on to create a formal PHP specification.\n\nBy November 2020, with PHP 8 being released later in the month, over 40% are still on outdated PHP 5; two out of every three websites using PHP are still on discontinued PHP versions,[14] and almost half of all PHP websites use version 5.6 or older, that not even Debian supports (while Debian 9 still supports version 7.0 and 7.1, those versions are unsupported by The PHP Development Team). In addition, PHP version 7.2, the most popular supported PHP version, will stop getting security updates on November 30, 2020[14] and therefore unless PHP websites are upgraded to version 7.3 (or newer), 84% of PHP websites will thus use discontinued versions.',
            'hero' => 'php.png',
            'bg_image' => 'php-photo.png',
            'year' => 2018,
            'rating' => 3,
            'views' => 42465,
            'video_path' => 'https://mcg-test-videos.s3.eu-west-2.amazonaws.com/file_example_WEBM_1920_3_7MB.webm'
        ],
        [
            'name' => 'Laravel for Beginners',
            'description' => "The Laravel framework has a few system requirements. All of these requirements are satisfied by the Laravel Homestead virtual machine, so it's highly recommended that you use Homestead as your local Laravel development environment.

However, if you are not using Homestead, you will need to make sure your server meets the following requirements:

PHP >= 7.3
BCMath PHP Extension
Ctype PHP Extension
Fileinfo PHP Extension
JSON PHP Extension
Mbstring PHP Extension
OpenSSL PHP Extension
PDO PHP Extension
Tokenizer PHP Extension
XML PHP Extension",
            'hero' => 'laravel.png',
            'bg_image' => 'laravel-photo.png',
            'year' => 2020,
            'rating' => 4.2,
            'views' => 300,
            'video_path' => 'https://mcg-test-videos.s3.eu-west-2.amazonaws.com/SampleVideo_1280x720_30mb.mp4'
        ],
        [
            'name' => 'VueJs Course',
            'description' => "Vue (pronounced /vjuː/, like view) is a progressive framework for building user interfaces. Unlike other monolithic frameworks, Vue is designed from the ground up to be incrementally adoptable. The core library is focused on the view layer only, and is easy to pick up and integrate with other libraries or existing projects. On the other hand, Vue is also perfectly capable of powering sophisticated Single-Page Applications when used in combination with modern tooling and supporting libraries.

If you’d like to learn more about Vue before diving in, we created a video walking through the core principles and a sample project.

If you are an experienced frontend developer and want to know how Vue compares to other libraries/frameworks, check out the Comparison with Other Frameworks.",
            'hero' => 'vue.png',
            'bg_image' => 'vue-photo.png',
            'year' => 2020,
            'rating' => 2.1,
            'views' => 11,
            'video_path' => 'https://mcg-test-videos.s3.eu-west-2.amazonaws.com/SampleVideo_1280x720_30mb.mp4'
        ],
        [
            'name' => 'Docker',
            'description' => "Docker overview
Estimated reading time: 10 minutes

Docker is an open platform for developing, shipping, and running applications. Docker enables you to separate your applications from your infrastructure so you can deliver software quickly. With Docker, you can manage your infrastructure in the same ways you manage your applications. By taking advantage of Docker’s methodologies for shipping, testing, and deploying code quickly, you can significantly reduce the delay between writing code and running it in production.

The Docker platform
Docker provides the ability to package and run an application in a loosely isolated environment called a container. The isolation and security allow you to run many containers simultaneously on a given host. Containers are lightweight because they don’t need the extra load of a hypervisor, but run directly within the host machine’s kernel. This means you can run more containers on a given hardware combination than if you were using virtual machines. You can even run Docker containers within host machines that are actually virtual machines!

Docker provides tooling and a platform to manage the lifecycle of your containers:

Develop your application and its supporting components using containers.
The container becomes the unit for distributing and testing your application.
When you’re ready, deploy your application into your production environment, as a container or an orchestrated service. This works the same whether your production environment is a local data center, a cloud provider, or a hybrid of the two.
Docker Engine
Docker Engine is a client-server application with these major components:

A server which is a type of long-running program called a daemon process (the dockerd command).

A REST API which specifies interfaces that programs can use to talk to the daemon and instruct it what to do.

A command line interface (CLI) client (the docker command).",
            'hero' => 'docker.png',
            'bg_image' => 'docker-photo.png',
            'year' => 2018,
            'rating' => 2.2,
            'views' => 32,
            'video_path' => 'https://mcg-test-videos.s3.eu-west-2.amazonaws.com/small.webm'
        ],
        [
            'name' => 'Mysql',
            'description' => "About MySQL
MySQL is the world's most popular open source database. With its proven performance, reliability and ease-of-use, MySQL has become the leading database choice for web-based applications, used by high profile web properties including Facebook, Twitter, YouTube, Yahoo! and many more.

Oracle drives MySQL innovation, delivering new capabilities to power next generation web, cloud, mobile and embedded applications.",
            'hero' => 'mysql.png',
            'bg_image' => 'mysql-photo.png',
            'year' => 2017,
            'rating' => 5.8,
            'views' => 47,
            'video_path' => 'https://mcg-test-videos.s3.eu-west-2.amazonaws.com/small.webm'
        ]
    ]);
    }
}
