<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExperiencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('experiences', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->text('description')->nullable();
            $table->string('hero')->nullable();
            $table->string('bg_image')->nullable();
            $table->string('video_path')->nullable();
            $table->string('year', 4)->nullable();
            $table->double('rating')->nullable();
            $table->integer('views')->default(0);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));

            // Add some indexes on searchable columns
            $table->index('name');
            $table->index('year');
            $table->index('created_at');
            $table->index('updated_at');
        });

        // Full Text Index
        DB::statement('ALTER TABLE experiences ADD FULLTEXT fulltext_index (description)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('experiences');
    }
}
