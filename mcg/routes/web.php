<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'App\Http\Controllers\WelcomeController@index')->name('welcome');

Route::post('/experiences/{experience}/increment_views', 'App\Http\Controllers\ExperienceController@increment_views')->name('experiences.increment_views');
Route::post('/experiences/{experience}/toggle_favorite', 'App\Http\Controllers\ExperienceController@toggle_favorite')->name('experiences.toggle_favorite');
Route::resource('experiences', 'App\Http\Controllers\ExperienceController')->only(['index', 'show']);
