const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js/')
    .sass('resources/sass/app.scss', 'public/css')
    .copy([
        'node_modules/owl.carousel/dist/assets/owl.carousel.min.css',
        'node_modules/owl.carousel/dist/assets/owl.theme.default.min.css',
        'node_modules/easy-autocomplete/dist/easy-autocomplete.min.css'
    ], 'public/css')
    .copy([
        'node_modules/jquery/dist/jquery.min.js',
        'node_modules/popper.js/dist/umd/popper.min.js',
        'node_modules/bootstrap/dist/js/bootstrap.min.js',
        'node_modules/owl.carousel/dist/owl.carousel.min.js',
        'node_modules/easy-autocomplete/dist/jquery.easy-autocomplete.min.js',
        'resources/js/site.js',
        'resources/js/experience.js'
    ], 'public/js');
