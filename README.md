## Intro

Build a proof of concept tool using Laravel that allows users to find content and render associated resources: poster, content and video.

## Screenshots

#### Welcome page
The welcome page will display the latest 5 experiences in a carousel along with any available categories and associated experiences.

![Experiences carousel.](/Screenshots/experiences_page.png "Experiences carousel")

![Experience Categories.](/Screenshots/experience_categories.png "Experiences carousel")

#### Search Functionality
The page allows users to filter content based on their search criteria by the following fields:

* Experience Name
![Experience Name Search.](/Screenshots/experience_name_search.png "Experience Name Search")
* Experience Description
![Experience Description Search.](/Screenshots/partial_description_search.png "Experience Description Search")
* Experience Year
![Experience Year Search.](/Screenshots/year_search.png "Experience Year Search")
* Experience Rating
![Experience Rating Search.](/Screenshots/rating_search.png "Rating Search")
* Category Name
![Category Name Search.](/Screenshots/partial_category_name_search.png "Category Name Search")

#### Individual Experience Page
The experience page will stream the video content from a AWS S3 bucket using the [VideoJS](https://videojs.com/) HTML5 player framework.

![Experience Page.](/Screenshots/experience_page.png "Experience Video Playback")

It will also display experiences from the same category.

![Linked Experiences.](/Screenshots/linked_experiences.png "Linked Experiences")

## Infrastructure
This proof of concept runs on 3 docker containers
* Web container running nginx with node (node:14.2-alpine && nginx:1.18-alpine) as
* App container running php 7.3 from php:7.3-fpm. In hindsight I should have probably tried out php 8
* Db container running mysql 5.7 from mysql:5.7. Again this should have been mysql 8 but SequelPro did not want to connect to the host of 127.0.0.1 even with the correct port exposed inside docker.

## Prerequisites

In order to deploy this webapp, the host machine should have
[Docker and Docker compose](https://docs.docker.com/compose/) installed.

## How to

```bash
$ git clone git@github.com:alexlungu/mcg.git mcg
$ cd mcg
$ make init
```

Then head over to http://localhost

This repo also includes a useful [Makefile](https://github.com/alexlungu/mcg/blob/master/Makefile) I tend to include with all projects to help with various commands that may be required on deployment.


## Long Grass / Ideas
* Add user registration and authentication routes
* Add user roles and enable premium content based on Auth Guards
* Track user viewing history and stats
* Implement user favourite toggle
* Design and create a dashboard to allow uploading of new experiences and relevant content
* Define AWS S3 IAM role with read permissions instead of making the bucket public
* Standardise images by resizing, creating thumbnails and cropping images by center. This should really happen at dashboard upload stage.
* Replace blade files with Vue components
* Cypress.JS frontend testing
* Unit Tests
* Geolocation dependant content
* Analytics (Logstash + Filebeats + Kibana / Grafana springs to mind for the access and error logs)
